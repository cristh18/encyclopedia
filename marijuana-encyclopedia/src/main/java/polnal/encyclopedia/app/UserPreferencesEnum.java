package polnal.encyclopedia.app;

/**
 * Created by Cristhian on 4/9/2016.
 */
public enum UserPreferencesEnum {

    PATH_CONFIG_FILE("PATH_CONFIG_FILE");

    private String key;

    private UserPreferencesEnum(String key){
        this.key=key;
    }

    public String key() {
        return key;
    }
}
