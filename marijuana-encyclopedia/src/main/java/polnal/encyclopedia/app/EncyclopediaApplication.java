package polnal.encyclopedia.app;

import java.util.WeakHashMap;

/**
 * Created by Cristhian on 4/9/2016.
 */
public class EncyclopediaApplication {

    private static EncyclopediaApplication encyclopediaInstance = new EncyclopediaApplication();

    private EncyclopediaApplication() {

    }

    public static EncyclopediaApplication getInstance() {
        return encyclopediaInstance;
    }

    private WeakHashMap<String,Object> map = new WeakHashMap<String,Object>();

    public void put(String key, Object o) {
        map.put(key, o);
    }

    public Object get(String key) {
        return map.get(key);
    }

    public void resetCache(){
        map.clear();
    }
}
