package polnal.encyclopedia.controller;

import polnal.encyclopedia.data.CategoryRepository;
import polnal.encyclopedia.data.DocumentRepository;
import polnal.encyclopedia.model.Category;
import polnal.encyclopedia.model.Document;
import polnal.encyclopedia.util.FileDirectoryUtil;
import polnal.encyclopedia.util.PDFSearchContentUtil;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by cristhian on 25/03/16.
 */
@Model
public class SearchController {

	@Inject
	private CategoryRepository categoryRepository;

	@Inject
	private DocumentRepository documentRepository;

	@Produces
	@Named
	private List<Category> categoriesList;

	@Produces
	@Named
	private List<Document> documentList;

	@Produces
	@Named
	private Category categorySelected;

	@Produces
	@Named
	private String searchWord;

	@PostConstruct
	public void initNewSearch() {
		try {
			this.searchWord = new String("");
			this.documentList = new ArrayList<Document>();
			this.loadCategories();
			if (this.categoriesList == null) {
				this.categoriesList = new ArrayList<Category>();
			}

			Map<String, String> params = getExternalContext().getRequestParameterMap();
			if (params.get("categoryId") != null && !params.get("categoryId").trim().equals("")) {
				if (!loadCategory(Long.parseLong(params.get("categoryId")))) {
					FacesContext context = FacesContext.getCurrentInstance();
					HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
					response.sendRedirect("index.jsf");
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 *
	 */
	private void loadCategories() {
		this.categoriesList = categoryRepository.findAllCategoriesByName();
	}

	private boolean loadCategory(Long id) throws IOException {
		boolean answer = false;
		FileDirectoryUtil.getKnowledgeFile();
		if (!this.categoriesList.isEmpty()) {
			for (Category category : this.categoriesList) {
				if (answer = category.getId().equals(id)) {
					this.categorySelected = category;
					this.categorySelected.setDocuments(documentRepository.findAllByCategoryId(category));
					break;
				}
			}
		}
		return answer;
	}

	public List<Category> getCategoriesList() {
		return categoriesList;
	}

	public List<Document> getDocumentList() {
		return documentList;
	}

	public String getSearchWord() {
		return searchWord;
	}

	public void setSearchWord(String searchWord) {
		this.searchWord = searchWord;
	}

	public Category getCategorySelected() {
		return categorySelected;
	}

	/**
	 *
	 */
	public String findDocuments() {
		if (this.searchWord != null && !this.searchWord.trim().equals("")) {
			try {
				File directory = FileDirectoryUtil.getKnowledgeFile();
				Set<String> keySet = new HashSet<String>();

				// Búsqueda por frase completa
				// Consulta por titulo
				Set<Document> temp = new HashSet<Document>(documentRepository.findAllByFieldLike("title", searchWord));
				// Consulta por palabras clave
				temp.addAll(documentRepository.findAllByFieldLike("keyWords", searchWord));
				// Consulta por resumen
				temp.addAll(documentRepository.findAllByFieldLike("resume", searchWord));
				// Búsqueda por palabra
				String[] array = searchWord.split(" ");
				for (String word : array) {
					temp.addAll(documentRepository.findAllByFieldLike("title", word));
					temp.addAll(documentRepository.findAllByFieldLike("keyWords", word));
					temp.addAll(documentRepository.findAllByFieldLike("resume", word));
				}
				for (Document document : temp) {
					keySet.add(document.getPath());
				}

				// Consulta por ruta
				List<String> pathList = PDFSearchContentUtil.consultar(directory, searchWord, keySet);
				for (String path : pathList) {
					List<Document> documents = documentRepository.findAllByFieldEqual("path", path);
					if (!documents.isEmpty()) {
						temp.add(documents.get(0));
					}
				}
				this.documentList.addAll(temp);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return "result";
	}

	public String getPathFromSelectedDoc() {
		String selectedDocument = "";
		try {
			Map<String, String> params = getExternalContext().getRequestParameterMap();
			selectedDocument = params.get("selectedDocument");
			FileDirectoryUtil.getKnowledgeFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return selectedDocument;
	}

	private ExternalContext getExternalContext() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		return facesContext.getExternalContext();
	}
}
