package polnal.encyclopedia.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Category implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1288925261761683813L;
	private Long id;
	private String code;
	private String name;
	private String description;
	private String path;
	private List<Document> documents;

	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the code
	 */
	@Column(length = 3, unique = false, precision = 0, scale = 0, nullable = true, insertable = true, updatable = true)
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the name
	 */
	@Column(length = 100, unique = false, precision = 0, scale = 0, nullable = true, insertable = true, updatable = true)
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	@Column(length = 1000, unique = false, precision = 0, scale = 0, nullable = true, insertable = true, updatable = true)
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the path
	 */
	@Column(length = 100, unique = false, precision = 0, scale = 0, nullable = true, insertable = true, updatable = true)
	public String getPath() {
		return path;
	}

	/**
	 * @param path
	 *            the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * ------------------------------------------
	 * 
	 * @todo add comment for javadoc
	 *
	 * @generated
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "category", cascade = CascadeType.REFRESH)
	public List<Document> getDocuments() {
		if (this.documents == null) {
			this.documents = new ArrayList<Document>();
		}
		return documents;
	}

	/** @generated */
	public void setDocuments(final List<Document> documents) {
		this.documents = documents;
	}

	/**
	 * Associate Category with DeliveryPoint
	 * 
	 * @generated
	 */
	public void addDocuments(Document document) {
		if (document == null) {
			return;
		}
		getDocuments().add(document);
		document.setCategory(this);
	}

	/**
	 * Unassociate Category from DeliveryPoint
	 * 
	 * @generated
	 */
	public void removeDocument(Document document) {
		if (document == null) {
			return;
		}
		getDocuments().remove(document);
		document.setCategory(null);
	}

	/**
	 * @generated
	 */
	public void removeAllDocuments() {
		List<Document> remove = new ArrayList<Document>();
		remove.addAll(getDocuments());
		for (Document element : remove) {
			removeDocument(element);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Category other = (Category) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		return true;
	}

}
