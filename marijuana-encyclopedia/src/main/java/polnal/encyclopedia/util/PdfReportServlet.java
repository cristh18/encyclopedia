package polnal.encyclopedia.util;

import polnal.encyclopedia.app.EncyclopediaApplication;
import polnal.encyclopedia.app.UserPreferencesEnum;

import javax.enterprise.context.SessionScoped;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * Created by Cristhian on 4/7/2016.
 */
@WebServlet("/report.pdf")
@SessionScoped
public class PdfReportServlet extends HttpServlet {

    private static final String MEDIA_TYPE = "application/pdf";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String shortPathFile = request.getParameter("path");
        String longPathFile = (String) EncyclopediaApplication.getInstance().get(UserPreferencesEnum.PATH_CONFIG_FILE.key());
        String pathFile = longPathFile.concat(File.separator).concat(shortPathFile);
        byte[] content = FileDirectoryUtil.getBytesFromFile(pathFile);
        response.setContentType(MEDIA_TYPE);
        response.setContentLength(content.length);
        response.getOutputStream().write(content);
    }


}
