package polnal.encyclopedia.util;

import polnal.encyclopedia.app.EncyclopediaApplication;
import polnal.encyclopedia.app.UserPreferencesEnum;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by Cristhian on 4/9/2016.
 */
public class FileDirectoryUtil {

    private static final String FILE_NAME = "fileName";

    public static File getKnowledgeFile() throws IOException {
        // Ruta directorio raiz archivos
        String fileName = Resources.getMessageResource(FILE_NAME, null, Resources.MESSAGES_BASE_NAME);

        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
                .getContext();
        String root = servletContext.getRealPath("/");

        File configurationFile = new File(root.concat(File.separator).concat(fileName));
        File directory = null;
        if (configurationFile.exists()) {
            Properties properties = new Properties();
            properties.load(new FileInputStream(configurationFile));
            directory = new File(properties.getProperty(FILE_NAME));
            EncyclopediaApplication.getInstance().put(UserPreferencesEnum.PATH_CONFIG_FILE.key(), directory.getAbsolutePath());
        }
        return directory;
    }

    public static byte[] getBytesFromFile(String path) {
        File file = new File(path);
        byte[] bytes;
        try {
            FileInputStream fis = new FileInputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];

            for (int readNum; (readNum = fis.read(buf)) != -1; ) {
                bos.write(buf, 0, readNum);
                System.out.println("read " + readNum + " bytes,");
            }
            bytes = bos.toByteArray();
        } catch (IOException ex) {
            ex.printStackTrace();
            bytes = null;
        }
        return bytes;
    }

}
