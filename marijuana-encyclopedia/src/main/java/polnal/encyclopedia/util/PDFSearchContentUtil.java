package polnal.encyclopedia.util;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PDFSearchContentUtil {

	/**
	 * Consulta la lista de archivos que contienen una frase especìfica o una o
	 * varias palabras de ésta
	 *
	 * @param directorio
	 *            Corresponde al directorio en el que se encuentran los archivos
	 *            a consultar
	 * @param consulta
	 *            Frase que se quiere validar
	 * @param rutasIgnorar
	 *            Corresponde a las rutas de archivos que no se deben consultr
	 *            por ya haberse analizado
	 * @return Listado de nombres de archivos que cumplen con la búsqueda
	 * @throws IOException
	 */
	public static List<String> consultar(File directorio, String consulta, Set<String> rutasIgnorar)
			throws IOException {
		List<String> resultado = new ArrayList<String>();
		if (directorio != null && directorio.exists()) {
			File[] listaDirectorio = directorio.listFiles();
			for (File archivo : listaDirectorio) {
				if (archivo.isDirectory()) {
					resultado.addAll(consultar(archivo, consulta, rutasIgnorar));
				} else {
					if (!rutasIgnorar.contains(archivo.getParentFile().getName() + "/" + archivo.getName())
							&& validarContenido(archivo, consulta)) {
						resultado.add(archivo.getParentFile().getName() + "/" + archivo.getName());
					}
				}
			}
		}
		return resultado;
	}

	/**
	 * Determina si en el contenido de un archivo se encuentra la frase indicada
	 * o por lo menos una de las palabras que
	 *
	 * @param archivo
	 * @param consulta
	 * @return
	 */
	private static boolean validarContenido(File archivo, String consulta) {
		boolean contenidoValido = false;
		PDDocument document = null;
		try {
			document = PDDocument.load(archivo);
			PDFTextStripper stripper = new PDFTextStripper();
			stripper.setSortByPosition(false);
			stripper.setStartPage(1);
			StringBuffer buffer = new StringBuffer(stripper.getText(document));
			contenidoValido = buffer.toString().toLowerCase().contains(consulta.toLowerCase());
			if (!contenidoValido) {
				String[] terminosConsulta = consulta.split(" ");
				for (String termino : terminosConsulta) {
					if (contenidoValido = buffer.toString().toLowerCase().contains(termino.toLowerCase().trim())) {
						break;
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (document != null) {
				try {
					document.close();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		}
		return contenidoValido;
	}
/*
	public static String getKnowledgeBaseDirectory() {
		String homeDir = System.getProperty("user.home");
		String knowledgeBaseDirectory = homeDir.concat("/").concat("MARIHUANA/");
		System.out.println("OS current User home directory directory is " + homeDir);
		System.out.println("OS current knowledge Base Directory is " + knowledgeBaseDirectory);
		return knowledgeBaseDirectory;
	}

	public static void main(String[] args) {
		String nameDirectory = "000-Generalidades";
		String ruta = getKnowledgeBaseDirectory().concat(nameDirectory);
		System.out.println(ruta);
		PDFSearchContentUtil searchContent = new PDFSearchContentUtil();
		File directorio = new File(ruta);
		// System.out.println(searchContent.consultar(directorio, "marihuana"));
	}*/
	
	public static void main(String[] args) {
		try {
			System.out.println(consultar(new File("/Users/arevalo/Documents/MARIHUANADOCS"), "perspectiva", new HashSet<String>()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
