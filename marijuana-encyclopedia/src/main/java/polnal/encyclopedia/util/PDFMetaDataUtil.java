package polnal.encyclopedia.util;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.xmpbox.xml.XmpParsingException;
import polnal.encyclopedia.dto.DocumentDto;

import java.io.File;
import java.io.IOException;
import java.util.List;


public class PDFMetaDataUtil {

    public static DocumentDto documentDto;

    public static DocumentDto getDocument(String documentName) {
        documentDto = new DocumentDto();
        PDDocument document = null;
        try {
            File file = new File(documentName);
            document = PDDocument.load(file);

            PDFTextStripper stripper = new PDFTextStripper();
            stripper.setSortByPosition(false);
            stripper.setStartPage(1);
            stripper.setEndPage(2);
            documentDto.setDocumentName(file.getName());
            documentDto.setPagesNumber(document.getNumberOfPages());
            documentDto.setDocumentText(stripper.getText(document));
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (document != null) {
                try {
                    document.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return documentDto;
    }

    /*public static void getDataFromDirectory(String pathDirectory, List<String> keyWords) {
        File directory = new File(PDFSearchContentUtil.getKnowledgeBaseDirectory().concat("000-Generalidades"));
//        File directory = new File(PDFSearchContentUtil.getDefaultUserHomeDirectory().concat("/").concat(pathDirectory);
        if (directory.exists()) {
            File[] fileList = directory.listFiles();
            for (File file : fileList) {
                if (!file.isDirectory()) {
                    DocumentDto documentDto = getDocument(file.getAbsolutePath());
                    System.out.println(documentDto.getDocumentText());
                }
            }
        }
    }*/

    /*
    public static void getDataFromFile(String pathFile) {
//        File file = new File(PDFSearchContentUtil.getKnowledgeBaseDirectory().concat("000-Generalidades/002-Abuso.pdf"));
        File file = new File(PDFSearchContentUtil.getKnowledgeBaseDirectory().concat("/").concat(pathFile));
        getDocument(file.getAbsolutePath());
    }*/

    public static DocumentDto getDocumentDto() {
        return documentDto;
    }

    public static void setDocumentDto(DocumentDto documentDto) {
        PDFMetaDataUtil.documentDto = documentDto;
    }

    /*public static void main(String[] args) throws IOException,
            XmpParsingException {
        getDataFromDirectory(null, null);

    }*/
}
