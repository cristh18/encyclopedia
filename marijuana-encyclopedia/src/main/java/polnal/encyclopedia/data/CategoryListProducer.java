package polnal.encyclopedia.data;

import polnal.encyclopedia.model.Category;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * @author Cristhian
 */
@RequestScoped
public class CategoryListProducer {

    @Inject
    private CategoryRepository categoryRepository;

    /**
     *
     */
    private List<Category> categoryList;

    /**
     * @return the clients
     */
    @Produces
    @Named
    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void onCategoryListChanged(
            @Observes(notifyObserver = Reception.IF_EXISTS) final Category category) {
        retrieveAllCategoryListOrderedByName();
    }

    @PostConstruct
    public void retrieveAllCategoryListOrderedByName() {
        categoryList = categoryRepository.findAllCategoriesByName();
    }

}
