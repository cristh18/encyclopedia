package polnal.encyclopedia.data;

import polnal.encyclopedia.model.Category;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Stateless
public class CategoryRepository {

	@Inject
	private EntityManager em;

	/**
	 * Get all items from order
	 *
	 * @return
	 */
	public List<Category> findAllCategoriesByName() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Category> criteria = cb.createQuery(Category.class);
		Root<Category> category = criteria.from(Category.class);
		criteria.select(category).orderBy(cb.asc(category.get("name")));
		return em.createQuery(criteria).getResultList();
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Category findById(Long id) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Category> criteria = cb.createQuery(Category.class);
		Root<Category> category = criteria.from(Category.class);
		Predicate categoryById = cb.equal(category.get("id"), id);
		criteria.select(category).where(categoryById);
		return em.createQuery(criteria).getSingleResult();
	}
}
