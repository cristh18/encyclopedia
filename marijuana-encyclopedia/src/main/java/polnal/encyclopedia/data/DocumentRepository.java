package polnal.encyclopedia.data;

import polnal.encyclopedia.model.Category;
import polnal.encyclopedia.model.Document;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.List;

@Stateless
public class DocumentRepository {

	@Inject
	private EntityManager em;

	public Document findById(Long id) {
		return em.find(Document.class, id);
	}

	public List<Document> findAllOrderedByField(String field) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Document> criteria = cb.createQuery(Document.class);
		Root<Document> document = criteria.from(Document.class);
		criteria.select(document).orderBy(cb.asc(document.get(field)));
		return em.createQuery(criteria).getResultList();
	}

	public List<Document> findAllByCategoryId(Category category) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Document> criteria = cb.createQuery(Document.class);
		Root<Document> document = criteria.from(Document.class);
		document.fetch("category", JoinType.LEFT);
		Predicate documentyByCategory = cb.equal(document.get("category"), category);
		criteria.select(document).where(documentyByCategory);
		return em.createQuery(criteria).getResultList();
	}

	public List<Document> findAllByFieldLike(String fieldName, String fieldValue) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Document> criteria = cb.createQuery(Document.class);
		Root<Document> document = criteria.from(Document.class);
		Predicate documents = cb.like(document.get(fieldName).as(String.class), "%" + fieldValue + "%");
		criteria.select(document).where(documents);
		return em.createQuery(criteria).getResultList();
	}

	public List<Document> findAllByFieldEqual(String fieldName, String fieldValue) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Document> criteria = cb.createQuery(Document.class);
		Root<Document> document = criteria.from(Document.class);
		Predicate documents = cb.equal(document.get(fieldName).as(String.class), fieldValue);
		criteria.select(document).where(documents);
		return em.createQuery(criteria).getResultList();
	}

	// public List<Document> findAllOrderedByName() {
	// CriteriaBuilder cb = em.getCriteriaBuilder();
	// CriteriaQuery<Document> criteria = cb.createQuery(Document.class);
	// Root<Document> document = criteria.from(Document.class);
	// criteria.select(document).orderBy(cb.asc(document.get("name")));
	// return em.createQuery(criteria).getResultList();
	// }
	//
	//
	//
	// public List<Document> findAllByKeyWord(String keyWord) {
	// CriteriaBuilder cb = em.getCriteriaBuilder();
	// CriteriaQuery<Document> criteria = cb.createQuery(Document.class);
	// Root<Document> document = criteria.from(Document.class);
	// Predicate documents = cb.like(document.get("keyWords").as(String.class),
	// "%" + keyWord + "%");
	// criteria.select(document).where(documents);
	// return em.createQuery(criteria).getResultList();
	// }
	//
	// public List<Document> findAllByResume(String phrase) {
	// CriteriaBuilder cb = em.getCriteriaBuilder();
	// CriteriaQuery<Document> criteria = cb.createQuery(Document.class);
	// Root<Document> document = criteria.from(Document.class);
	// Predicate documents = cb.like(document.get("resume").as(String.class),
	// "%" + phrase + "%");
	// criteria.select(document).where(documents);
	// return em.createQuery(criteria).getResultList();
	// }
	//
	// public List<Document> findAllByPath(String phrase) {
	// CriteriaBuilder cb = em.getCriteriaBuilder();
	// CriteriaQuery<Document> criteria = cb.createQuery(Document.class);
	// Root<Document> document = criteria.from(Document.class);
	// Predicate documents = cb.equal(document.get("path").as(String.class), "%"
	// + phrase + "%");
	// criteria.select(document).where(documents);
	// return em.createQuery(criteria).getResultList();
	// }

}
