package polnal.encyclopedia.converter;

import org.primefaces.component.selectonemenu.SelectOneMenu;
import polnal.encyclopedia.model.Category;

import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.util.Collection;

@FacesConverter(value = "categoryConverter")
public class CategoryConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component,
                              String value) {
        if (component instanceof SelectOneMenu) {
            for (UIComponent child : component.getChildren()) {
                if (child instanceof UISelectItems) {
                    UISelectItems selectItems = (UISelectItems) child;
                    for (Category selectItemValue : (Collection<Category>) selectItems
                            .getValue()) {
                        if (selectItemValue instanceof Category) {
                            String id = "" + selectItemValue.getId();
                            if (value.equals(id)) {
                                return selectItemValue;
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        String str = "";
        if (value instanceof Category) {
            str = "" + ((Category) value).getId();
        }
        return str;
    }

}