package polnal.encyclopedia.converter;

import polnal.encyclopedia.data.DocumentRepository;
import polnal.encyclopedia.model.Document;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

@FacesConverter(value = "documentConverter")
public class DocumentConverter implements Converter {

    @Inject
    private DocumentRepository documentRepository;

    @Override
    public Document getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }
        try {
            Document obj = documentRepository
                    .findById(Long.valueOf(value));
            System.out.println(obj.getId());
            return obj;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ConverterException(new FacesMessage(String.format(
                    "Cannot convert %s to Document", value)), e);
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (!(value instanceof Document)) {
            return null;
        }
        String s = String.valueOf(((Document) value).getId());
        return s;
    }


}
